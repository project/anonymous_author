<?php

use Drupal\comment\CommentInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * @file
 * Documentation for anonymous_author API.
 */

/**
 * Alter anonymous_author email notifications.
 *
 * @param array $params
 *   An associative parameters array for the email, with the following elements:
 *   - 'entity': Contains the entity for which we're sending the email.
 *   - 'subject': The subject of the email.
 *   - 'message': The contents of the email.
 * @param array $context
 *   An associate array with the following elements:
 *   - 'type': The type of change, "update" or "comment"
 *   - 'entity': The entity for the update.
 *   - 'name': The name of the user.
 *   - 'email': The email of the user.
 *   - 'comment': The comment entity, if $type is "comment" (NULL otherwise).
 */
function hook_anonymous_author_notification_alter(array &$params, array $context) {
  if ($context['entity']->getEntityTypeId() !== 'node') {
    return;
  }
  if ($context['entity']->bundle() !== 'forum') {
    return;
  }
  if ($context['type'] === 'update') {
    $options = ['langcode' => $langcode];
    $params['subject'] = t(
      'Forum updated: @title',
      ['@title' =>  $context['entity']->label()],
      $options
    );
  }
}

/**
 * Alter anonymous_author email notifications for a specific type.
 *
 * TYPE can be either "update" or "comment".
 *
 * @param array $params
 *   An associative parameters array for the email, with the following elements:
 *   - 'entity': Contains the entity for which we're sending the email.
 *   - 'subject': The subject of the email.
 *   - 'message': The contents of the email.
 * @param array $context
 *   An associate array with the following elements:
 *   - 'type': The type of change, "update" or "comment"
 *   - 'entity': The entity for the update.
 *   - 'name': The name of the user.
 *   - 'email': The email of the user.
 *   - 'comment': The comment entity, if $type is "comment" (NULL otherwise).
 */
function hook_anonymous_author_notification_TYPE_alter(array &$params, array $context) {
  if ($context['entity']->getEntityTypeId() !== 'node') {
    return;
  }
  if ($context['entity']->bundle() !== 'forum') {
    return;
  }
  $params['subject'] = t(
    'New comment on your forum: @title',
    ['@title' =>  $context['entity']->label()]
  );
}

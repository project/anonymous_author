<?php

namespace Drupal\anonymous_author\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Formatter for anonymous author.
 *
 * @FieldFormatter(
 *   id = "anonymous_author",
 *   label = @Translation("Anonymous author"),
 *   field_types = {
 *     "anonymous_author",
 *   }
 * )
 */
class AnonymousAuthorFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = ['#markup' => $item->name];
    }

    return $elements;
  }

}

<?php

namespace Drupal\anonymous_author\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Render\Element\Email;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Provides a field type for an anonymous author.
 *
 * @FieldType(
 *   id = "anonymous_author",
 *   label = @Translation("Anonymous author"),
 *   default_formatter = "anonymous_author",
 *   default_widget = "anonymous_author",
 * )
 */
class AnonymousAuthorItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'email' => [
          'type' => 'varchar',
          'length' => Email::EMAIL_MAX_LENGTH,
        ],
        'name' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'notify' => [
          'type' => 'int',
          'size' => 'tiny',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['email'] = DataDefinition::create('email')
      ->setLabel(t('Author email'));
    $properties['name'] = DataDefinition::create('string')
      ->setLabel(t('Author name'));
    $properties['notify'] = DataDefinition::create('boolean')
      ->setLabel(t('Notify'));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
    $constraints = parent::getConstraints();

    $constraints[] = $constraint_manager->create('ComplexData', [
      'email' => [
        'Length' => [
          'max' => Email::EMAIL_MAX_LENGTH,
          'maxMessage' => $this->t(
            '%name: the email address can not be longer than @max characters.',
            [
              '%name' => $this->getFieldDefinition()->getLabel(),
              '@max' => Email::EMAIL_MAX_LENGTH,
            ]
          ),
        ],
      ],
    ]);

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = new Random();
    $values['email'] = $random->name() . '@example.com';
    $values['name'] = $random->name();
    $values['notify'] = mt_rand(0, 1);
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $empty_email = $this->email === NULL || $this->email === '';
    $empty_name = $this->name === NULL || $this->name === '';
    $empty_notify = $this->notify === NULL;
    return $empty_email && $empty_name && $empty_notify;
  }

}

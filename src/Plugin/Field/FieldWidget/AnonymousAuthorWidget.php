<?php

namespace Drupal\anonymous_author\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Render\Element\Email;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Widget for the anonymous author.
 *
 * @FieldWidget(
 *   id = "anonymous_author",
 *   label = @Translation("Anonymous Author"),
 *   field_types = {
 *     "anonymous_author"
 *   }
 * )
 */
class AnonymousAuthorWidget extends WidgetBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->currentUser = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'email_placeholder' => '',
      'name_placeholder' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['email_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email placeholder'),
      '#default_value' => $this->getSetting('email_placeholder'),
      '#description' => $this->t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];
    $element['name_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name placeholder'),
      '#default_value' => $this->getSetting('name_placeholder'),
      '#description' => $this->t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $email_placeholder = $this->getSetting('email_placeholder');
    if (!empty($email_placeholder)) {
      $summary[] = $this->t('Email placeholder: @email_placeholder', ['@email_placeholder' => $email_placeholder]);
    }
    else {
      $summary[] = $this->t('No email placeholder');
    }

    $name_placeholder = $this->getSetting('name_placeholder');
    if (!empty($name_placeholder)) {
      $summary[] = $this->t('Name placeholder: @name_placeholder', ['@name_placeholder' => $name_placeholder]);
    }
    else {
      $summary[] = $this->t('No name placeholder');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    /* Hide this form if:
     * - this is a new entity AND the user is NOT anonymous
     * - this is an existing entity AND the user DOES NOT have permission to edit it
     * If there is an entity id, then this is an edit form
     */
    $entity_id = $form_state->getformObject()->getEntity()->id();
    if ((!isset($entity_id) && !$this->currentUser->isAnonymous()) || (
      isset($entity_id) &&
      !$this->currentUser->hasPermission('edit anonymous author fields')
    )) {
      return;
    }

    $element['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#default_value' => !empty($items[$delta]->email) ? $items[$delta]->email : '',
      '#placeholder' => $this->getSetting('email_placeholder'),
      '#required' => $this->fieldDefinition->isRequired(),
      '#size' => 60,
      '#maxlength' => Email::EMAIL_MAX_LENGTH,
    ];

    $element['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#default_value' => !empty($items[$delta]->name) ? $items[$delta]->name : '',
      '#placeholder' => $this->getSetting('name_placeholder'),
      '#required' => $this->fieldDefinition->isRequired(),
      '#size' => 60,
    ];

    $element['notify'] = [
      '#type' => 'checkbox',
      '#default_value' => !empty($items[$delta]->notify),
      '#title' => $this->t('Notify me of updates and comments to this post.'),
    ];

    return $element;
  }

}
